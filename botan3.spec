%define _unpackaged_files_terminate_build 0
%define debug_package %nil

%define api %(echo %{version} |cut -d. -f1)
%define major %(echo %{version} |cut -d. -f2)

Name:           botan3
Summary:        Crypto library written in C++
Version:        3.3.0
Release:        1
Group:          System/Libraries
License:        BSD
Url:            http://botan.randombit.net/
Source0:        Botan-3.3.0.tar.xz


BuildRequires: python3 python3-devel
BuildRequires: pkgconfig(bzip2)
BuildRequires: pkgconfig(gmp)
BuildRequires: openssl
BuildRequires: pkgconfig(zlib)
BuildRequires: pkgconfig(liblzma)
BuildRequires: gcc-c++ = 12.2.0-1
BuildRequires: openssl-devel
# For man page (rst2man)
BuildRequires: python-docutils
Requires: libstdc++ >= 11.0.0

%description
Botan is a BSD-licensed crypto library written in C++. It provides a
wide variety of basic cryptographic algorithms, X.509 certificates and
CRLs, PKCS \#10 certificate requests, a filter/pipe message processing
system, and a wide variety of other features, all written in portable
C++. The API reference, tutorial, and examples may help impart the
flavor of the library.

%package -n lib%{name}
Summary:        Main library for botan3
Group:          System/Libraries

%description -n lib%{name}
Botan is a BSD-licensed crypto library written in C++. It provides a
wide variety of basic cryptographic algorithms, X.509 certificates and
CRLs, PKCS \#10 certificate requests, a filter/pipe message processing
system, and a wide variety of other features, all written in portable
C++. The API reference, tutorial, and examples may help impart the
flavor of the library.

%package -n python3-%{name}
Summary:        Python3 lib for botan3
Group:          Development/Python

%description -n python3-%{name}
Python module for botan.

%package -n %{name}-devel
Summary:        Development files for botan3
Group:          Development/Other

%description -n %{name}-devel
This package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n Botan-%{version}

find . -name "*.c" -o -name "*.h" -o -name "*.cpp" | xargs chmod 0644

%build
# we have the necessary prerequisites, so enable optional modules
%define enable_modules bzip2,zlib,lzma

# fixme: maybe disable unix_procs, very slow.
%define disable_modules proc_walk,unix_procs,sqlite3

./configure.py \
	--prefix=%{_prefix} \
	--libdir=%{_lib} \
	--os=linux \
	--cpu=%{_arch} \
	--enable-modules=%{enable_modules} \
	--disable-modules=%{disable_moudles}

%make_build

%install
%make_install

DESTDIR="%{buildroot}"

rm -f %{buildroot}%{_libdir}/*.a

%check
%ifnarch %{ix86}
export LD_LIBRARY_PATH="$(pwd)"
./botan-test ||:
%endif

%files
%doc %{_docdir}/botan-%{version}/*.txt
%{_libdir}/libbotan-%{api}.so.%{major}*
%{_bindir}/botan
%{_mandir}/man1/botan.1.gz

%files -n lib%{name}
%doc %{_docdir}/botan-%{version}/*.txt
%{_libdir}/libbotan-%{api}.so.%{major}*

%files -n %{name}-devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/Botan-%{version}
%doc %{_docdir}/botan-%{version}/handbook
%doc %{_docdir}/botan-%{version}/*.txt
%doc %{_mandir}/man1/*.1*

%files -n python3-%{name}
%doc %{_docdir}/botan-%{version}/*.txt
%{python3_sitearch}/%{name}.py
%{python3_sitearch}/__pycache__/*

%changelog
* Thu Apr 18 2024 liaohongke <liaohongke@huawei.com>
- Botan 3.3.0 init
